Small demonstrating application for WorkFusion test task. Makes fixes on incorrect chars in predefined sequences.

Build application:

``gradle installDist`` - this command builds app, and creates distribution

then

``cd build/install/sequence-checker/bin`` - go to executables folder

usage:

``./sequence-checker >> out.csv`` - launches app with generating test/train data and writing output to csv file

OR you can specify input test and train files (both are required).

``./sequence-checker train.csv test.csv >> out.csv``

train file format: comma-separated incorrect-correct pairs without header
~~~
330j3EVCWBI2tLtuu755T302,33063EVCWBI24L466755T302
nQE7vv12CtGC2i7t4919t2B9,6QE75512C4GC2074491942B9
YN8JZE8UsD365WT3X69X27KA,YN8JZE8U5D365WT3X69X27KA
W511mAK46UmNS3Fm9Vs302U5,W5116AK46U6NS3F69V5302U5
~~~

test file format: one sequence per line, without header

~~~
V9570PIu2P326LI327GxG706
PmEYzNFmMv585mWHv8mz6mYF
T1Kw7R5ww0W3K8wVM5B1w7b2
WP7S73mD76V4EWMw197wmwLZ
J15K2GyG923F39S384VBR899
P6DFN8R89xpxAUUNEVCN81p4
~~~