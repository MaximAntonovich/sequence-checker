package by.mant.testgen;

import by.mant.util.Const;
import by.mant.util.StringPair;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by maxim on 5.1.17.
 */
public class TestDataGeneratorTest {
    private TestDataGenerator generator = new TestDataGenerator();
    private Pattern p = Pattern.compile(Const.REGEX);
    private String correctString;

    @Before
    public void setCorrectString() {
        correctString = generator.getCorrectString();
    }

    @Test
    public void testGenerateCorrectString() {
        assertTrue(p.matcher(correctString).matches());
    }

    @Test
    public void testGenerateIncorrectString() {
        assertFalse(p.matcher(generator.getIncorrectString(correctString)).matches());
    }

    @Test
    public void testGenerateTrainData() {
        int n = 10;
        List<StringPair> trainData = generator.generateTrainData(n);
        assertTrue(trainData.stream().allMatch((pair) -> p.matcher(pair.correct).matches()));
        assertTrue(trainData.stream().noneMatch((pair) -> p.matcher(pair.incorrect).matches()));
    }

    @Test
    public void testGenerateTestData() {
        int n = 10;
        List<String> testData = generator.generateTestData(n);
        assertTrue(testData.stream().noneMatch(s -> p.matcher(s).matches()));
    }
}
