package by.mant.testgen;

import by.mant.util.Const;
import by.mant.util.StringPair;
import com.mifmif.common.regex.Generex;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class for generating test and train data. Uses Generex library for generating random string matching the regex.
 */
public class TestDataGenerator {
    private static final Generex GENEREX = new Generex(Const.REGEX);
    private static final Random RANDOM = new Random(Const.SEED);

    public String getCorrectString() {
        return GENEREX.random();
    }

    public String getIncorrectString(String correctString) {
        int mistakesCount = RANDOM.nextInt(6) + 1;
        String incorrectString = correctString;
        for (int i = 0; i < mistakesCount; i++) {
            incorrectString = incorrectString.replace(correctString.charAt(RANDOM.nextInt(24)), (char) (RANDOM.nextInt(28) + 'a'));
        }
        return incorrectString;
    }

    public List<StringPair> generateTrainData(int n) {
        List<StringPair> pairs = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            String correct = getCorrectString();
            String incorrect = getIncorrectString(correct);
            pairs.add(new StringPair(correct, incorrect));
        }
        return pairs;
    }

    public List<String> generateTestData(int n) {
        List<String> incorrectStrings = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            incorrectStrings.add(getIncorrectString(getCorrectString()));
        }
        return incorrectStrings;
    }
}
