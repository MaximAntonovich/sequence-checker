package by.mant.checker;

import by.mant.util.StringPair;

import java.util.List;

/**
 * Root of all checkers. Defines base methods for training and testing.
 */
public interface Checker {

    void train(List<StringPair> trainData);

    List<StringPair> check(List<String> testData);
}
