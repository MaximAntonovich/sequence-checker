package by.mant.checker;

import by.mant.util.Const;
import by.mant.util.StringPair;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains some useful staff for character statistics gathering.
 */
public abstract class AbstractChecker implements Checker {
    protected Map<Character, Map<Character, Integer>> errorsInfo = new LinkedHashMap<>();
    protected StringBuilder sb = new StringBuilder();


    /**
     * Memoizes the occurrence of correct-incorrect character
     *
     * @param right
     * @param wrong
     */
    protected void memoizeChar(char right, char wrong) {
        if (!errorsInfo.containsKey(wrong)) {
            errorsInfo.put(wrong, new LinkedHashMap<>());
            errorsInfo.get(wrong).put(right, 1);
        } else {
            if (errorsInfo.get(wrong).containsKey(right)) {
                errorsInfo.get(wrong).put(right, errorsInfo.get(wrong).get(right) + 1);
            } else {
                errorsInfo.get(wrong).put(right, 1);
            }
        }
    }

    /**
     *
     * @see by.mant.checker.implementation.NaiveChecker
     */
    protected void memoizeWrongChars(List<StringPair> trainData) {
        trainData.forEach((stringPair -> {
            char[] correctArray = stringPair.correct.toCharArray();
            char[] incorrectArray = stringPair.incorrect.toCharArray();
            for (int i = 0; i < Const.LENGTH; i++) {
                if (correctArray[i] != incorrectArray[i]) {
                    memoizeChar(correctArray[i], incorrectArray[i]);
                }
            }
        }));
    }


}
