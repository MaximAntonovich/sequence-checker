package by.mant.checker.implementation;

import by.mant.checker.AbstractChecker;
import by.mant.util.StringPair;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.mant.util.Const.CORRECT_CHARS;

/**
 * Naive checker class based on incorrect-correct character entrances counting.
 */
public class NaiveChecker extends AbstractChecker {

    /**
     * Training method. Counts entrances of correct-incorrect character pairs.
     *
     * @param trainData list of pairs of correct-incorrect sequence
     */
    @Override
    public void train(List<StringPair> trainData) {
        memoizeWrongChars(trainData);
    }

    /**
     *  Test method. Error fixing based on gathered statistics. Replaces incorrect char by mostly occurring correct character.
     * @param testData
     * @return list of pairs of incorrect-fixed sequences.
     */
    @Override
    public List<StringPair> check(List<String> testData) {
        return testData.stream().map(this::fixIncorrectString).collect(Collectors.toList());
    }

    /**
     * Makes incorrect-correct character replacement for one string. Used as map function in check method.
     * @param incorrectString
     * @return
     */
    private StringPair fixIncorrectString(String incorrectString) {
        sb.setLength(0);
        for (char c : incorrectString.toCharArray()) {
            if (CORRECT_CHARS.indexOf(c) != -1) {
                sb.append(c);
            } else {
                if (errorsInfo.containsKey(c)) {
                    Map<Character, Integer> charInfo = errorsInfo.get(c);
                    char replacingChar = charInfo.entrySet()
                            .stream()
                            .max(Comparator.comparingInt(Map.Entry::getValue))
                            .map(Map.Entry::getKey)
                            .orElse(c);
                    sb.append(replacingChar);
                } else {
                    sb.append(c);
                }
            }
        }
        return new StringPair(sb.toString(), incorrectString);
    }

}
