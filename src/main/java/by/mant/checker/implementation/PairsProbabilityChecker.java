package by.mant.checker.implementation;

import by.mant.checker.AbstractChecker;
import by.mant.util.StringPair;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.mant.util.Const.*;

/**
 * Checker based on Neighbours probability(frequence) count. Replacing incorrect character rule is finding more frequent character with same neighbours.
 */
public class PairsProbabilityChecker extends AbstractChecker {

    private Map<CharPair, Map<Character, Integer>> pairFrequencies = new LinkedHashMap<>();

    /**
     * Train method. Counts entrances of <code>(neighbours, correct char)</code>.
     *
     * @param trainData list of pairs of correct-incorrect sequence
     */
    @Override
    public void train(List<StringPair> trainData) {
        for (StringPair pair : trainData) {
            char[] correctChars = pair.correct.toCharArray();
            char[] incorrectChars = pair.incorrect.toCharArray();
            for (int i = 0; i < LENGTH; i++) {
                if (correctChars[i] != incorrectChars[i]) {
                    switch (i) {
                        case 0:
                            memoizeCharPair(new CharPair(NO_CHAR, incorrectChars[i + 1]), correctChars[i]);
                            break;
                        case LENGTH - 1:
                            memoizeCharPair(new CharPair(incorrectChars[i - 1], NO_CHAR), correctChars[i]);
                            break;
                        default:
                            memoizeCharPair(new CharPair(incorrectChars[i - 1], incorrectChars[i + 1]), correctChars[i]);
                            break;
                    }
                }
            }
        }
    }

    /**
     *  Test method. Fixes incorrect chars in sequence. Based on Pairs frequency principle.
     * @param testData list of incorrect sequences
     * @return list of pairs of incorrect-fixed sequence
     */
    @Override
    public List<StringPair> check(List<String> testData) {
        return testData.stream().map(incorrectString -> {
            sb.setLength(0);
            char[] stringChars = incorrectString.toCharArray();
            for (int i = 0; i < LENGTH; i++) {
                if (CORRECT_CHARS.indexOf(stringChars[i]) != -1) {
                    sb.append(stringChars[i]);
                } else {
                    sb.append(correctChar(i, stringChars));
                }
            }
            return new StringPair(sb.toString(), incorrectString);
        }).collect(Collectors.toList());
    }

    /**
     * Retrieves correct character from gathered statistics for given index of incorrect character
     * @param index index of incorrect character
     * @param incorrectStringChars characters of incorrect sequence
     * @return correct character
     */
    private char correctChar(int index, char[] incorrectStringChars) {
        CharPair key;
        switch (index) {
            case 0:
                key = new CharPair(NO_CHAR, incorrectStringChars[1]);
                break;
            case LENGTH - 1:
                key = new CharPair(incorrectStringChars[LENGTH - 2], NO_CHAR);
                break;
            default:
                key = new CharPair(incorrectStringChars[index - 1], incorrectStringChars[index + 1]);
                break;
        }
        return pairFrequencies.get(key) != null ? pairFrequencies.get(key).entrySet()
                .stream()
                .max(Comparator.comparingInt(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .orElse(incorrectStringChars[index]) : incorrectStringChars[index];
    }

    /**
     *  Collects occurrence of pair <code>(neighbours, correct character)</code>
     * @param pair pair of neighbour characters
     * @param right correct character from correct sequence
     */
    private void memoizeCharPair(CharPair pair, char right) {
        if (pairFrequencies.containsKey(pair)) {
            Map<Character, Integer> rightChars = pairFrequencies.get(pair);
            if (rightChars.containsKey(right)) {
                rightChars.put(right, rightChars.get(right) + 1);
            } else {
                rightChars.put(right, 1);
            }
        } else {
            Map<Character, Integer> rightChars = new LinkedHashMap<>();
            rightChars.put(right, 1);
            pairFrequencies.put(pair, rightChars);
        }
    }

    /**
     * Inner class to store pair of neighbours of character in sequence.
     */
    private final static class CharPair {
        final char before;
        final char after;

        CharPair(char before, char after) {
            this.before = before;
            this.after = after;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CharPair charPair = (CharPair) o;

            return before == charPair.before && after == charPair.after;
        }

        @Override
        public int hashCode() {
            int result = (int) before;
            result = 31 * result + (int) after;
            return result;
        }
    }
}
