package by.mant.checker.implementation;

import by.mant.checker.AbstractChecker;
import by.mant.util.StringPair;
import es.usc.citius.hipster.algorithm.Hipster;
import es.usc.citius.hipster.graph.GraphBuilder;
import es.usc.citius.hipster.graph.GraphSearchProblem;
import es.usc.citius.hipster.graph.HipsterDirectedGraph;
import es.usc.citius.hipster.model.impl.WeightedNode;
import es.usc.citius.hipster.model.problem.SearchProblem;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.mant.util.Const.*;

/**
 * Bayesian network implementation class. Library Hipster4j used for creating directional acyclic weighted graph and path searching on it.
 */
public class BayesianNetworkChecker extends AbstractChecker {
    private Map<Integer, Map<Character, Map<Character, Double>>> frequenciesInformation = new LinkedHashMap<>();
    private HipsterDirectedGraph<Node, Double> bayesianNetwork;

    /**
     * Train method. Gatheres information about character-character links and builds the graph.
     *
     * @param trainData list of pairs correct-incorrect sequence
     */
    @Override
    public void train(List<StringPair> trainData) {
        List<String> correctSequences = trainData.stream().map(pair -> pair.correct).collect(Collectors.toList());
        correctSequences.forEach(sequence -> {
            char[] sequenceChars = sequence.toCharArray();
            for (int i = 0; i < LENGTH - 1; i++) {
                if (frequenciesInformation.containsKey(i)) {
                    if (frequenciesInformation.get(i).containsKey(sequenceChars[i])) {
                        if (frequenciesInformation.get(i).get(sequenceChars[i]).containsKey(sequenceChars[i + 1])) {
                            frequenciesInformation.get(i).get(sequenceChars[i])
                                    .put(sequenceChars[i + 1], frequenciesInformation.get(i).get(sequenceChars[i]).get(sequenceChars[i + 1]) + 1.0);
                        } else {
                            frequenciesInformation.get(i).get(sequenceChars[i]).put(sequenceChars[i + 1], 1.0);
                        }
                    } else {
                        frequenciesInformation.get(i).put(sequenceChars[i], new LinkedHashMap<>());
                        frequenciesInformation.get(i).get(sequenceChars[i]).put(sequenceChars[i + 1], 1.0);
                    }
                } else {
                    frequenciesInformation.put(i, new LinkedHashMap<>());
                    frequenciesInformation.get(i).put(sequenceChars[i], new LinkedHashMap<>());
                    frequenciesInformation.get(i).get(sequenceChars[i]).put(sequenceChars[i + 1], 1.0);
                }
            }
        });
        GraphBuilder<Node, Double> graphBuilder = GraphBuilder.create();
        for (int i = 0; i < LENGTH - 1; i++) {
            for (char c1 : CORRECT_CHARS.toCharArray()) {
                if (frequenciesInformation.containsKey(i)) {
                    if (frequenciesInformation.get(i).containsKey(c1)) {
                        for (char c2 : CORRECT_CHARS.toCharArray()) {
                            if (frequenciesInformation.get(i).get(c1).containsKey(c2)) {
                                graphBuilder.connect(new Node(i, c1))
                                        .to(new Node(i + 1, c2))
                                        .withEdge(frequenciesInformation.get(i).get(c1).get(c2));
                            } else {
                                graphBuilder.connect(new Node(i, c1))
                                        .to(new Node(i + 1, c2))
                                        .withEdge(0.0D);
                            }
                        }
                    }
                }
            }
        }
        Node firstFictiveNode = new Node(-1, NO_CHAR);
        Node lastFictiveNode = new Node(LENGTH, NO_CHAR);
        for (char c : CORRECT_CHARS.toCharArray()) {
            graphBuilder.connect(firstFictiveNode).to(new Node(0, c)).withEdge(0.0D);
            graphBuilder.connect(new Node(LENGTH - 1, c)).to(lastFictiveNode).withEdge(0.0D);
        }
        bayesianNetwork = graphBuilder.createDirectedGraph();
    }

    /**
     * Test method. Fixes the wrong chars based on training-gathered statistics.
     * @param testData - list of incorrect sequences
     * @return list of pair correct-incorrect sequences
     */
    @Override
    public List<StringPair> check(List<String> testData) {
        return testData.stream().map(incorrectString -> {
            sb.setLength(0);
            int wrongSequenceIndex = 0;
            int wrongSequenceLength = 0;
            for (int i = 0; i < LENGTH; i++) {
                if (CORRECT_CHARS.indexOf(incorrectString.charAt(i)) != -1) {
                    sb.append(incorrectString.charAt(i));
                    if (wrongSequenceLength != 0) {
                        if (wrongSequenceIndex == 0) {
                            char[] correctChars = getCorrectChars(new Node(-1, NO_CHAR), new Node(i, incorrectString.charAt(i)), wrongSequenceLength);
                            sb.insert(wrongSequenceIndex, correctChars);
                        } else {
                            char[] correctChars = getCorrectChars(new Node(wrongSequenceIndex - 1, incorrectString.charAt(wrongSequenceIndex - 1)), new Node(i, incorrectString.charAt(i)), wrongSequenceLength);
                            sb.insert(wrongSequenceIndex, correctChars);
                        }
                        wrongSequenceIndex = 0;
                        wrongSequenceLength = 0;
                    }
                } else {
                    if (wrongSequenceLength == 0) {
                        wrongSequenceIndex = i;
                    }
                    wrongSequenceLength++;
                    if (i == LENGTH - 1) {
                        char[] correctChars = getCorrectChars(new Node(wrongSequenceIndex - 1, incorrectString.charAt(wrongSequenceIndex - 1)), new Node(LENGTH, NO_CHAR), wrongSequenceLength);
                        sb.insert(wrongSequenceIndex, correctChars);
                    }
                }
            }
            return new StringPair(sb.toString(), incorrectString);
        }).collect(Collectors.toList());
    }

    /**
     * Returns characters from optimal path.
     * @param start - Start node
     * @param finish - Finish node
     * @param wrongSequenceLength - length of sequence of incorrect characters
     * @return array of correct chars based on optimal path searching
     */
    private char[] getCorrectChars(Node start, Node finish, int wrongSequenceLength) {
        char[] correctChars = new char[wrongSequenceLength];
        List<Node> path = getOptimalPath(start, finish);
        for (int j = 1; j < path.size() - 1; j++) {
            correctChars[j - 1] = path.get(j).nodeChar;
        }
        return correctChars;
    }

    /**
     *  Searches the optimal path between two nodes. Dijkstra algorithm used. <code>-log(w + 1)</code> transform needed to transform
     *  max-product to min-sum problem.
     * @param start Start node
     * @param finish Finish node
     * @return List of nodes of optimal path
     */
    private List<Node> getOptimalPath(Node start, Node finish) {
        SearchProblem<Double, Node, WeightedNode<Double, Node, Double>> problem = GraphSearchProblem
                .startingFrom(start)
                .in(bayesianNetwork)
                .extractCostFromEdges((weight) -> -Math.log(weight + 1))
                .build();
        return Hipster.createDijkstra(problem).search(finish).getOptimalPaths().get(0);
    }

    /**
     * Inner class to represent position and character in sequence
     */
    private final static class Node {
        final int index;
        final char nodeChar;

        public Node(int index, char nodeChar) {
            this.index = index;
            this.nodeChar = nodeChar;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;

            return index == node.index && nodeChar == node.nodeChar;
        }

        @Override
        public int hashCode() {
            int result = index;
            result = 31 * result + (int) nodeChar;
            return result;
        }
    }
}
