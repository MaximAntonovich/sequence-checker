package by.mant;

import by.mant.checker.Checker;
import by.mant.checker.implementation.BayesianNetworkChecker;
import by.mant.checker.implementation.NaiveChecker;
import by.mant.checker.implementation.PairsProbabilityChecker;
import by.mant.testgen.TestDataGenerator;
import by.mant.util.StringPair;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static by.mant.util.Const.NUMBER_OF_TEST_SAMPLES;
import static by.mant.util.Const.NUMBER_OF_TRAIN_SAMPLES;

/**
 * Main class. Performs application flow.
 */
public class Main {
    /**
     * Creates all checkers and shows the results of their work.
     *
     * @param args command-line args (optional filenames).
     */
    public static void main(String[] args) {
        List<StringPair> trainData;
        List<String> testData;

        if (args.length != 0) {
            if (args.length == 2) {
                trainData = getTrainDataFromFile(args[0]);
                testData = getTestDataFromFile(args[1]);
            } else {
                System.out.println("Something wrong with your command line arguments.");
                return;
            }
            if (testData == null || trainData == null) {
                return;
            }
        } else {
            TestDataGenerator generator = new TestDataGenerator();
            trainData = generator.generateTrainData(NUMBER_OF_TRAIN_SAMPLES);
            testData = generator.generateTestData(NUMBER_OF_TEST_SAMPLES);
        }

        Checker naiveChecker = new NaiveChecker();
        naiveChecker.train(trainData);
        List<StringPair> naiveCheckResults = naiveChecker.check(testData);

        Checker pairsProbabilityChecker = new PairsProbabilityChecker();
        pairsProbabilityChecker.train(trainData);
        List<StringPair> pairsProbabilityCheckResults = pairsProbabilityChecker.check(testData);

        Checker bayesianNetworkChecker = new BayesianNetworkChecker();
        bayesianNetworkChecker.train(trainData);
        List<StringPair> bayesianNetworkCheckResults = bayesianNetworkChecker.check(testData);

        System.out.println("Incorrect,Naive,PairProbability,BayesianNetwork");
        for (int i = 0; i < NUMBER_OF_TEST_SAMPLES; i++) {
            System.out.println(testData.get(i) + "," + naiveCheckResults.get(i).correct + "," + pairsProbabilityCheckResults.get(i).correct + "," + bayesianNetworkCheckResults.get(i).correct);
        }
    }

    private static List<String> getTestDataFromFile(String filename) {
        try {
            return Files.lines(Paths.get(filename)).collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Test file doesn't exist");
            return null;
        }
    }

    private static List<StringPair> getTrainDataFromFile(String filename) {
        try {
            return Files.lines(Paths.get(filename)).map(line -> new StringPair(line.split(",")[1], line.split(",")[0])).collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Train file doesn't exist");
            return null;
        }
    }


}
