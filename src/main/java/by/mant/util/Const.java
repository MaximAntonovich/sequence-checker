package by.mant.util;

/**
 * Some self-describing constants
 */
public class Const {
    public static final String REGEX = "[0-9A-Z]{24}";
    public static final long SEED = 5_01_2017L;
    public static final int LENGTH = 24;
    public static final int NUMBER_OF_TRAIN_SAMPLES = 1_000_000;
    public static final int NUMBER_OF_TEST_SAMPLES = 100;
    public static final String CORRECT_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public static final char NO_CHAR = '-';
}
