package by.mant.util;

/**
 * Utility class for storing correct-incorrect sequence pair.
 */
public class StringPair {
    public String correct;
    public String incorrect;

    public StringPair(String correct, String incorrect) {
        this.correct = correct;
        this.incorrect = incorrect;
    }
}
